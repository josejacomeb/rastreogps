<?php

$dias = 180; //60 dias maximo que permanezcan los datos
date_default_timezone_set('America/Guayaquil');
if (!is_null($_GET["id"])) {
    $id = $_GET["id"];
}
if (!is_null($_GET["lat"])) {
    $latitud = $_GET["lat"];
}
if (!is_null($_GET["long"])) {
    $longitud = $_GET["long"];
}
if (!is_null($_GET["opcion"])) {
    $opcion = $_GET["opcion"];
}
if (!is_null($id) && !is_null($latitud) && !is_null($longitud)) {
    require __DIR__ . '/vendor/autoload.php';

    $options = array(
        'cluster' => 'us2',
        'useTLS' => true
    );
    $pusher = new Pusher\Pusher(
            'a0b9a4160e4b7ceab1ef',
            'd9c82547688b6f84c2ad',
            '719014',
            $options
    );

    $config = parse_ini_file('../db.ini');
    try {
        $dbh = new PDO('mysql:dbname=' . $config['db'] . ';host=' . $config['servername'] . ';charset=utf8mb4', $config['username'], $config['password']);
        if (!is_null($opcion)) {
            $valores = escapeshellcmd(getcwd() . "/cliente.py " . (string) $id . " " . (string) $opcion);
            $message = shell_exec($valores);
            echo $message;
            $stmt = $dbh->prepare("INSERT INTO registro (fecha, evento, auto, latitud, longitud) VALUES (:fecha, 'isw:id', ':auto', 0, 0)");
            $stmt->execute(array(':fecha' => $fecha, ':id' => substr($opcion, 0, 1), ':auto' => $id));
        } else {
            $stmt = $dbh->prepare("INSERT INTO registro (fecha, evento, auto, latitud, longitud) VALUES (:fecha, 'ira',:id,:latitud,:longitud)");
            $stmt->execute(array(':fecha' => $fecha, ':id' => $id, ':latitud' => $latitud, ':longitud' => $longitud));
            $data['mensaje'] = 'NuevoDato';
            $data['id'] = $id;
            $data['latitud'] = $latitud;
            $data['longitud'] = $longitud;
            $pusher->trigger('cambiodatos', 'auto', $data);
        }
        $stmt = null;
        $stmta = $dbh->prepare("DELETE FROM registro WHERE fecha < DATE_SUB(NOW(), INTERVAL :dias DAY)");
        $stmta->execute(array(':dias' => $dias));
        $stmta = null;
    } catch (Exception $e) {
        error_log($e->getMessage());
        exit('Error de conexión'); //something a user can understand
    }
}