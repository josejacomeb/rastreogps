<?php

session_start();
if ($_SESSION["usuario"] === NULL) {
    echo "Acceso denegado";
} else {
    $dir_subida = getcwd() . '/imagenes/';
    if (isset($_FILES['fichero_usuario']['name'])) {
        $fichero_subido = $dir_subida . basename($_FILES['fichero_usuario']['name']);
    }
    if (isset($_GET['q'])) {
        $q = $_GET['q'];
    }
    if (isset($_GET["di"])) {
        $diainicio = intval($_GET['di']);
    }
    if (isset($_GET['mi'])) {
        $mesinicio = intval($_GET['mi']);
    }
    if (isset($_GET['ai'])) {
        $añoinicio = intval($_GET['ai']);
    }
    if (isset($_GET['df'])) {
        $diafinal = intval($_GET['df']);
    }
    if (isset($_GET['mf'])) {
        $mesfinal = intval($_GET['mf']);
    }
    if (isset($_GET['af'])) {
        $añofinal = intval($_GET['af']);
    }
    if (isset($_GET['opcion'])) {
        $opcion = $_GET['opcion'];
    }

    if (isset($q)) {
        $config = parse_ini_file('../db.ini');
// Create connection
        $conn = new mysqli($config['servername'], $config['username'], $config['password'], $config['db']);
        $conn->set_charset("utf8");

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        if ($q == "*") {
            $sql = "SELECT * FROM registro ";
        } else {
            $sql = "SELECT * FROM registro WHERE auto='" . $q . "'";
        }
        if (isset($opcion)) {
            if (strpos($sql, 'WHERE') !== false) {
                $sql = $sql . " AND";
            } else {
                $sql = $sql . " WHERE";
            }
            $sql = $sql . " evento='" . $opcion . "'";
        }
        #var_dump(isset($añofinal, $añoinicio, $mesinicio, $mesfinal, $diainicio, $diafinal));
        if (isset($añofinal, $añoinicio, $mesinicio, $mesfinal, $diainicio, $diafinal)) {
            if (strpos($sql, 'WHERE') !== false) {
                $sql = $sql . " AND";
            } else {
                $sql = $sql . " WHERE";
            }
            $sql = $sql . " CAST(fecha as date) BETWEEN '" . $añoinicio . "-" . $mesinicio . "-" . $diainicio
                    . "' AND '" . $añofinal . "-" . $mesfinal . "-" . $diafinal . "'";
        }

        $sql = $sql . " ORDER BY fecha DESC";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $objetoarray = array();
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $objetoarray[] = $row;
                }
                echo json_encode($objetoarray);
            } else {
                echo "<p class=\"w3-text-white\">Sin datos actualmente</p>";
            }
        } else {
            echo "<p class=\"w3-text-white\">Sin datos actualmente</p>";
        }
        $conn->close();
    }
}
