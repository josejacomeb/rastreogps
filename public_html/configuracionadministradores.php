<?php

session_start();
if ($_SESSION["usuario"] === NULL) {
    echo "Acceso denegado";
} else {
    if (isset($_GET['q'])) {
        $q = $_GET['q'];
    }
    if (isset($_GET['opcion'])) {
        $opcion = $_GET['opcion'];
    }
    if (isset($_GET['eliminar'])) {
        $eliminar = $_GET['eliminar'];
    }
    if (isset($_POST['agregar'])) {
        $agregar = $_POST['agregar'];
    }
    if (isset($_POST['modificar'])) {
        $modificar = $_POST['modificar'];
    }
    if (isset($q) || isset($opcion) || isset($eliminar) || isset($agregar) || isset($modificar)) {
        $config = parse_ini_file('../db.ini');
// Create connection
        $conn = new mysqli($config['servername'], $config['username'], $config['password'], $config['db']);
        $conn->set_charset("utf8");

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        if (isset($q)) {
            if ($q == "*") {
                $sql = "SELECT nombre FROM instructores ";
            } else if ($q == "todo*") {
                $sql = "SELECT * FROM instructores ";
            } else {
                $sql = "SELECT nombre FROM instructores WHERE nombre LIKE '" . $q . "%'";
            }
        }
        if (isset($opcion)) {
            $sql = "SELECT * FROM instructores WHERE nombre='" . $opcion . "'";
        }
        if (isset($eliminar)) {
            $result = $conn->query("SELECT foto from instructores WHERE nombre='" . $eliminar . "'");
            $ruta = $result->fetch_assoc()["foto"];
            $archivoeleminar = getcwd() . "/" . $ruta;
            unlink($archivoeleminar); // or die("No se puede eliminar el archivo");
            $sql = "DELETE from instructores WHERE nombre='" . $eliminar . "'";
        }
        if (isset($agregar)) {
            $sql = "INSERT INTO instructores (nombre, telefono, foto, auto, lunes,
            martes, miercoles, jueves, viernes, sabado) VALUES ('" . $agregar['nombre'] . "', 
 '" . $agregar['telefono'] . "', '" . $agregar['foto'] . "', " . $agregar['auto'] . ",'" .
                    json_encode($agregar['lunes']) . "', '" .
                    json_encode($agregar['martes']) . "', '" . json_encode($agregar['miercoles']) .
                    "','" . json_encode($agregar['jueves']) . "', '" . json_encode($agregar['viernes']) .
                    "','" . json_encode($agregar['sabado']) . "')";
        }
        if (isset($modificar)) {
            $sql = "UPDATE instructores" .
                    " SET telefono = '" . $modificar['telefono'] . "', foto = '" . $modificar['foto'] . "'," .
                    "auto = " . $modificar['auto'] . ", lunes = '" . json_encode($modificar['lunes']) .
                    "', martes = '" . json_encode($modificar['martes']) . "', miercoles = '" . json_encode($modificar['miercoles']) .
                    "', jueves = '" . json_encode($modificar['jueves']) . "', viernes = '" . json_encode($modificar['viernes']) .
                    "', sabado = '" . json_encode($modificar['sabado']) . "' WHERE nombre = '" . $modificar['nombre'] . "'";
        }
        $result = $conn->query($sql);

        if (isset($q) || isset($opcion)) {
            $objetoarray = array();
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $objetoarray[] = $row;
                }
                echo json_encode($objetoarray);
            } else {
                echo "<p class=\"w3-text-white\">Sin datos actualmente</p>";
            }
        }
        $conn->close();
    }
}
