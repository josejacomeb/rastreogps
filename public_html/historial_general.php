<?php
// Start the session
session_start();
if ($_SESSION["usuario"] === NULL) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Historial_General</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"/>
<link rel="icon" href="Imagenes_pagina/gps.png">
<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"></script>
<script src="leaflet.js"></script>
<style type="text/css">
body,td,th {
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
}
body {
    background-image: url(fondo.jpg);
    background-repeat: no-repeat;
	background-size: 100% 100%;
	background-attachment: fixed;
}
</style>
</head>
	
<body>
	
	<header>
	<div class="w3-bar w3-light-grey w3-center">
	<a style="width:30%" class="w3-bar-item w3-mobile"><img src="Imagenes_pagina/logo2.png" style="width:22%"/></a>
  	<a href="index.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>INICIO</b></a>
  	<a href="mapa.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>MAPA</b></a>
  	<a href="historial.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>HISTORIAL</b></a>
	<a href="administracion.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>ADMINISTRACIÓN</b></a>
  	<a href="acerca.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>ACERCA</b></a>
</div>
	</header>
	<container>
		<p style="font-size: 16px" class="w3-center w3-mobile w3-text-white"><b>Eliga el criterio de búsqueda:</b></p>
		<div class="w3-container w3-center">
  		<select id="seleccion" class="w3-mobile">
            <option value="*">Todos los autos</option>
            <option value="0">Auto 0</option>
            <option value="1">Auto 1</option>
            <option value="2">Auto 2</option>
            <option value="3">Auto 3</option>
            <option value="4">Auto 4</option>
            <option value="5">Auto 5</option>
            <option value="6">Auto 6</option>
            <option value="7">Auto 7</option>
            <option value="8">Auto 8</option>
            <option value="9">Auto 9</option>
        </select> 
        <span class="w3-text-white"><b>Fecha inicio:</b></span> 
        <input type="date" id="fechainicio" class="w3-mobile">
        <span class="w3-text-white"><b>Fecha final:</b></span> 
        <input type="date" id="fechafinal" class="w3-mobile">
        <select id="horario" class="w3-mobile">
            <option value="*">Todos los horarios</option>
            <option value="h1">Horario 1</option>
            <option value="h2">Horario 2</option>
            <option value="h3">Horario 3</option>
            <option value="h4">Horario 4</option>
            <option value="h5">Horario 5</option>
            <option value="h6">Horario 6</option>
            <option value="h7">Horario 7</option>
            <option value="h8">Horario 8</option>
        </select>
        <select id="instructor" class="w3-mobile">
        </select>
		</div>
		<br/>
		<div id="divtabla" class="w3-responsive" >Tabla </div>
	</container>
 		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="pusher.min.js"></script> 
        <script src="historial.js"></script>
</body></html>
