var cadena = "";
var comando = "";
var datos;
var usuario = {};
$(document).ready(function () {
    $("#alerta").hide();
    $("#subirfoto").hide();
    $("#nombre").val("");
    $("#telefono").val("");
    $("#resetearvalores").hide();
    $("#eliminarusuario").hide();
    $("#modificar").prop("disabled", false);
    $.get("configuracionadministradores.php?q=*", function (data, status) {
        $("#selectorInstructores").empty();
        $("#selectorInstructores").append('<option value="Nuevo Usuario"> Nuevo Usuario</option>');
        if (status === "success") {
            $("#alerta").hide();
            if (data.search("Sin datos") < 0) {
                datos = JSON.parse(data);
                datos.forEach(function (value) {
                    $("#selectorInstructores").append('<option value="' + value.nombre + '">' + value.nombre + '</option>');
                });
            }
        }
    });
    $("#resetearvalores").click(function () {
        setearelementos(false, true);
        $("#nombre").val("");
        $("#telefono").val("");
        $("#auto").val("");
        $("#imagen").prop("src", "imagenes/default.png");
    });
    $("#selectorInstructores").change(function () {
        if ($("#selectorInstructores").val() === "Nuevo Usuario") {
            setearelementos(false, true);
            $("#subirfoto").show();
            $("#nombre").val("");
            $("#telefono").val("");
            $("#imagen").prop("src", "imagenes/default.png");
        } else {
            $.get("configuracionadministradores.php?opcion=" + $("#selectorInstructores").val(), function (data, status) {
                if (status === "success") {
                    datos = JSON.parse(data);
                    $("#nombre").val(datos[0].nombre);
                    $("#telefono").val(datos[0].telefono);
                    if (datos[0].foto === null) {
                        $("#imagen").prop("src", "imagenes/default.png");
                    } else {
                        $("#imagen").prop("src", datos[0].foto);
                    }
                    $("#auto").val(datos[0].auto);
                    var dia = JSON.parse(datos[0].lunes);
                    $("#l1").prop("checked", JSON.parse(dia.h1));
                    $("#l2").prop("checked", JSON.parse(dia.h2));
                    $("#l3").prop("checked", JSON.parse(dia.h3));
                    $("#l4").prop("checked", JSON.parse(dia.h4));
                    $("#l5").prop("checked", JSON.parse(dia.h5));
                    $("#l6").prop("checked", JSON.parse(dia.h6));
                    $("#l7").prop("checked", JSON.parse(dia.h7));
                    $("#l8").prop("checked", JSON.parse(dia.h8));
                    dia = JSON.parse(datos[0].martes);
                    $("#ma1").prop("checked", JSON.parse(dia.h1));
                    $("#ma2").prop("checked", JSON.parse(dia.h2));
                    $("#ma3").prop("checked", JSON.parse(dia.h3));
                    $("#ma4").prop("checked", JSON.parse(dia.h4));
                    $("#ma5").prop("checked", JSON.parse(dia.h5));
                    $("#ma6").prop("checked", JSON.parse(dia.h6));
                    $("#ma7").prop("checked", JSON.parse(dia.h7));
                    $("#ma8").prop("checked", JSON.parse(dia.h8));
                    dia = JSON.parse(datos[0].miercoles);
                    $("#m1").prop("checked", JSON.parse(dia.h1));
                    $("#m2").prop("checked", JSON.parse(dia.h2));
                    $("#m3").prop("checked", JSON.parse(dia.h3));
                    $("#m4").prop("checked", JSON.parse(dia.h4));
                    $("#m5").prop("checked", JSON.parse(dia.h5));
                    $("#m6").prop("checked", JSON.parse(dia.h6));
                    $("#m7").prop("checked", JSON.parse(dia.h7));
                    $("#m8").prop("checked", JSON.parse(dia.h8));
                    dia = JSON.parse(datos[0].jueves);
                    $("#j1").prop("checked", JSON.parse(dia.h1));
                    $("#j2").prop("checked", JSON.parse(dia.h2));
                    $("#j3").prop("checked", JSON.parse(dia.h3));
                    $("#j4").prop("checked", JSON.parse(dia.h4));
                    $("#j5").prop("checked", JSON.parse(dia.h5));
                    $("#j6").prop("checked", JSON.parse(dia.h6));
                    $("#j7").prop("checked", JSON.parse(dia.h7));
                    $("#j8").prop("checked", JSON.parse(dia.h8));
                    dia = JSON.parse(datos[0].viernes);
                    $("#v1").prop("checked", JSON.parse(dia.h1));
                    $("#v2").prop("checked", JSON.parse(dia.h2));
                    $("#v3").prop("checked", JSON.parse(dia.h3));
                    $("#v4").prop("checked", JSON.parse(dia.h4));
                    $("#v5").prop("checked", JSON.parse(dia.h5));
                    $("#v6").prop("checked", JSON.parse(dia.h6));
                    $("#v7").prop("checked", JSON.parse(dia.h7));
                    $("#v8").prop("checked", JSON.parse(dia.h8));
                    dia = JSON.parse(datos[0].sabado);
                    $("#s1").prop("checked", JSON.parse(dia.h1));
                    $("#s2").prop("checked", JSON.parse(dia.h2));
                    $("#s3").prop("checked", JSON.parse(dia.h3));
                    $("#s4").prop("checked", JSON.parse(dia.h4));
                }
            });
        }
    });
    setearelementos(true, true);
    $("#telefono").keypress(function () {
        if (!$.isNumeric($("#telefono").val())) {
            $("#telefono").val($("#telefono").val().slice(0, -1));
            alertas("alert-warning", "Problema de tipo", "Coloque solo caracteres numericos en el campo teléfono", 3000);
        }
    });
    $("#nombre").keypress(function () {
        if ($.isNumeric($("#nombre").val())) {
            $("#nombre").val($("#nombre").val().slice(0, -1));
            alertas("alert-warning", "Problema de tipo", "Coloque solo caracteres alfabéticos en el campo nombre", 3000);
        }
    });
    $("#modificar").click(function () {
        if ($("#modificar").text() === "Modificar") {
            $("#modificar").text("Enviar cambios");
            $("#resetearvalores").show();
            $("#eliminarusuario").show();
        } else {
            var error = "";
            usuario = {};
            usuario.nombre = $("#nombre").val();
            usuario.telefono = $("#telefono").val();
            usuario.auto = $("#auto").val();
            if (usuario.nombre === "") {
                error += "No se puede añadir un usuario sin nombre\n";
            }
            if (usuario.telefono === "") {
                error += "No se puede añadir un usuario sin teléfono\n";
            }
            if (error === "") {
                if ($("#subirfoto").val() !== "") {
                    var forma = $('#formaimagen')[0]; // You need to use standard javascript object here
                    var datosForma = new FormData(forma);
                    $.ajax({
                        url: "/subirimagen.php",
                        type: "POST",
                        data: datosForma,
                        processData: false,
                        contentType: false,
                        async: false,
                        success: function (data) {
                            $("#imagen").prop("src", "imagenes/" + data);
                            usuario.foto = "imagenes/" + data;
                        }
                    });
                } else {
                    if ($("#selectorInstructores").text() !== "Nuevo Usuario") {
                        if (usuario.foto === "") {
                            usuario.foto = "imagenes/default.png";
                        }
                    } else {
                        usuario.foto = $("#imagen").attr("src");
                    }
                }
                var dia = {
                    h1: $("#l1").prop("checked"),
                    h2: $("#l2").prop("checked"),
                    h3: $("#l3").prop("checked"),
                    h4: $("#l4").prop("checked"),
                    h5: $("#l5").prop("checked"),
                    h6: $("#l6").prop("checked"),
                    h7: $("#l7").prop("checked"),
                    h8: $("#l8").prop("checked")
                };
                usuario.lunes = dia;
                var dia = {
                    h1: $("#ma1").prop("checked"),
                    h2: $("#ma2").prop("checked"),
                    h3: $("#ma3").prop("checked"),
                    h4: $("#ma4").prop("checked"),
                    h5: $("#ma5").prop("checked"),
                    h6: $("#ma6").prop("checked"),
                    h7: $("#ma7").prop("checked"),
                    h8: $("#ma8").prop("checked")
                };
                usuario.martes = dia;
                var dia = {
                    h1: $("#m1").prop("checked"),
                    h2: $("#m2").prop("checked"),
                    h3: $("#m3").prop("checked"),
                    h4: $("#m4").prop("checked"),
                    h5: $("#m5").prop("checked"),
                    h6: $("#m6").prop("checked"),
                    h7: $("#m7").prop("checked"),
                    h8: $("#m8").prop("checked")
                };
                usuario.miercoles = dia;
                var dia = {
                    h1: $("#j1").prop("checked"),
                    h2: $("#j2").prop("checked"),
                    h3: $("#j3").prop("checked"),
                    h4: $("#j4").prop("checked"),
                    h5: $("#j5").prop("checked"),
                    h6: $("#j6").prop("checked"),
                    h7: $("#j7").prop("checked"),
                    h8: $("#j8").prop("checked")
                };
                usuario.jueves = dia;
                var dia = {
                    h1: $("#v1").prop("checked"),
                    h2: $("#v2").prop("checked"),
                    h3: $("#v3").prop("checked"),
                    h4: $("#v4").prop("checked"),
                    h5: $("#v5").prop("checked"),
                    h6: $("#v6").prop("checked"),
                    h7: $("#v7").prop("checked"),
                    h8: $("#v8").prop("checked")
                };
                usuario.viernes = dia;
                var dia = {
                    h1: $("#s1").prop("checked"),
                    h2: $("#s2").prop("checked"),
                    h3: $("#s3").prop("checked"),
                    h4: $("#s4").prop("checked")
                };
                usuario.sabado = dia;
                $.ajax({
                    method: "GET",
                    url: "configuracionadministradores.php?q=todo*",
                    success: function (dato) {
                        if (dato.search("Sin datos") < 0) {
                            var datoJSON = JSON.parse(dato);
                            error = "";
                            for (var i = 0; i < datoJSON.length; i++) {
                                if ($("#selectorInstructores").val().search("Nuevo usuario") && datoJSON[i].nombre !== usuario.nombre) {
                                    if (datoJSON[i].nombre === usuario.nombre) {
                                        error += "Usuarios con los mismos nombres ";
                                    }
                                    if (datoJSON[i].telefono === usuario.telefono) {
                                        error += "Usuario con el mismo numero de telefono registrado ";
                                    }
                                    if (datoJSON[i].auto === usuario.auto) {
                                        var dia = JSON.parse(datoJSON[i].lunes);
                                        if ((JSON.parse(dia.h1) && usuario.lunes.h1) ||
                                                (JSON.parse(dia.h2) && usuario.lunes.h2) ||
                                                (JSON.parse(dia.h3) && usuario.lunes.h3) ||
                                                (JSON.parse(dia.h4) && usuario.lunes.h4) ||
                                                (JSON.parse(dia.h5) && usuario.lunes.h5) ||
                                                (JSON.parse(dia.h6) && usuario.lunes.h6) ||
                                                (JSON.parse(dia.h7) && usuario.lunes.h7) ||
                                                (JSON.parse(dia.h8) && usuario.lunes.h8)) {
                                            error += " Usuario " + datoJSON[i].nombre + " tiene el mismo horario el día lunes";
                                        }
                                        var dia = JSON.parse(datoJSON[i].martes);
                                        if ((JSON.parse(dia.h1) && usuario.martes.h1) ||
                                                (JSON.parse(dia.h2) && usuario.martes.h2) ||
                                                (JSON.parse(dia.h3) && usuario.martes.h3) ||
                                                (JSON.parse(dia.h4) && usuario.martes.h4) ||
                                                (JSON.parse(dia.h5) && usuario.martes.h5) ||
                                                (JSON.parse(dia.h6) && usuario.martes.h6) ||
                                                (JSON.parse(dia.h7) && usuario.martes.h7) ||
                                                (JSON.parse(dia.h8) && usuario.martes.h8)) {
                                            error += " Usuario " + datoJSON[i].nombre + " tiene el mismo horario el día martes";
                                        }
                                        var dia = JSON.parse(datoJSON[i].miercoles);
                                        if ((JSON.parse(dia.h1) && usuario.miercoles.h1) ||
                                                (JSON.parse(dia.h2) && usuario.miercoles.h2) ||
                                                (JSON.parse(dia.h3) && usuario.miercoles.h3) ||
                                                (JSON.parse(dia.h4) && usuario.miercoles.h4) ||
                                                (JSON.parse(dia.h5) && usuario.miercoles.h5) ||
                                                (JSON.parse(dia.h6) && usuario.miercoles.h6) ||
                                                (JSON.parse(dia.h7) && usuario.miercoles.h7) ||
                                                (JSON.parse(dia.h8) && usuario.miercoles.h8)) {
                                            error += " Usuario " + datoJSON[i].nombre + " tiene el mismo horario el día miercoles";
                                        }
                                        var dia = JSON.parse(datoJSON[i].jueves);
                                        if ((JSON.parse(dia.h1) && usuario.jueves.h1) ||
                                                (JSON.parse(dia.h2) && usuario.jueves.h2) ||
                                                (JSON.parse(dia.h3) && usuario.jueves.h3) ||
                                                (JSON.parse(dia.h4) && usuario.jueves.h4) ||
                                                (JSON.parse(dia.h5) && usuario.jueves.h5) ||
                                                (JSON.parse(dia.h6) && usuario.jueves.h6) ||
                                                (JSON.parse(dia.h7) && usuario.jueves.h7) ||
                                                (JSON.parse(dia.h8) && usuario.jueves.h8)) {
                                            error += " Usuario " + datoJSON[i].nombre + " tiene el mismo horario el día jueves";
                                        }
                                        var dia = JSON.parse(datoJSON[i].viernes);
                                        if ((JSON.parse(dia.h1) && usuario.viernes.h1) ||
                                                (JSON.parse(dia.h2) && usuario.viernes.h2) ||
                                                (JSON.parse(dia.h3) && usuario.viernes.h3) ||
                                                (JSON.parse(dia.h4) && usuario.viernes.h4) ||
                                                (JSON.parse(dia.h5) && usuario.viernes.h5) ||
                                                (JSON.parse(dia.h6) && usuario.viernes.h6) ||
                                                (JSON.parse(dia.h7) && usuario.viernes.h7) ||
                                                (JSON.parse(dia.h8) && usuario.viernes.h8)) {
                                            error += " Usuario " + datoJSON[i].nombre + " tiene el mismo horario el día viernes";
                                        }
                                        var dia = JSON.parse(datoJSON[i].sabado);
                                        if ((JSON.parse(dia.h1) && usuario.sabado.h1) ||
                                                (JSON.parse(dia.h2) && usuario.sabado.h2) ||
                                                (JSON.parse(dia.h3) && usuario.sabado.h3) ||
                                                (JSON.parse(dia.h4) && usuario.sabado.h4)) {
                                            error += " Usuario " + datoJSON[i].nombre + " tiene el mismo horario el día sabado";
                                        }
                                    }
                                }
                            }
                        }
                        if (error === "") {
                            if ($("#selectorInstructores").val() === "Nuevo Usuario") {
                                $.post("configuracionadministradores.php", {agregar: usuario}, "json");
                                alertas("alert-success", "Creación de usuario correcta", " El usuario " +
                                        usuario.nombre + " ha sido creado correctamente", 4000);
                                $.get("configuracionadministradores.php?q=*", function (data, status) {
                                    $("#selectorInstructores").empty();
                                    $("#selectorInstructores").append('<option value="Nuevo Usuario"> Nuevo Usuario </option>');
                                    if (status === "success") {
                                        $("#alerta").hide();
                                        datos = JSON.parse(data);
                                        datos.forEach(function (value) {
                                            $("#selectorInstructores").append('<option value="' + value.nombre + '">' + value.nombre + '</option>');
                                        });
                                    }
                                    $("#selectorInstructores").val(usuario.nombre);
                                });
                            } else {
                                $.post("configuracionadministradores.php", {modificar: usuario}, "json");
                                alertas("alert-success", "Modificación de usuario correcta", " El usuario " +
                                        usuario.nombre + " ha sido modificado correctamente", 4000);
                            }
                            $("#modificar").text("Modificar");
                            $("#resetearvalores").hide();
                            $("#eliminarusuario").hide();
                        } else {
                            alert("Errores encontrados" + error);
                            alertas("alert-danger", "Problemas al procesar los datos", error, 10000);
                        }
                    }
                });
            }

        }
        setearelementos(false, false);
        $("#subirfoto").show();
        $("#eliminarusuario").show();
    });
    $("#eliminarusuario").click(function () {
        alertas("alert-danger", "Eliminar usuario", "Va a eliminar el usuario " + $("#nombre").val(), 2000);
        if (confirm("Confirmar eliminación de usuario " + $("#nombre").val())) {
            alertas("alert-success", "Confirmación Eliminación", "Eliminado usuario " + $("#nombre").val(), 4000);
            $("#selectorInstructores option:selected").remove();
            $.get("configuracionadministradores.php?eliminar=" + $("#nombre").val());
            setearelementos(false, true);
            $("#nombre").val("");
            $("#telefono").val("");
            $("#auto").val("");
            $("#imagen").prop("src", "imagenes/default.png");
        } else {
            alertas("alert-info", "Eliminación Cancelada", "No se ha eliminado al usuario " + $("#nombre").val(), 4000);
        }
    });
});
function setearelementos(estado, resetear) {
    if (resetear) {
        $("#l1").prop("checked", false);
        $("#l2").prop("checked", false);
        $("#l3").prop("checked", false);
        $("#l4").prop("checked", false);
        $("#l5").prop("checked", false);
        $("#l6").prop("checked", false);
        $("#l7").prop("checked", false);
        $("#l8").prop("checked", false);
        $("#ma1").prop("checked", false);
        $("#ma2").prop("checked", false);
        $("#ma3").prop("checked", false);
        $("#ma4").prop("checked", false);
        $("#ma5").prop("checked", false);
        $("#ma6").prop("checked", false);
        $("#ma7").prop("checked", false);
        $("#ma8").prop("checked", false);
        $("#m1").prop("checked", false);
        $("#m2").prop("checked", false);
        $("#m3").prop("checked", false);
        $("#m4").prop("checked", false);
        $("#m5").prop("checked", false);
        $("#m6").prop("checked", false);
        $("#m7").prop("checked", false);
        $("#m8").prop("checked", false);
        $("#j1").prop("checked", false);
        $("#j2").prop("checked", false);
        $("#j3").prop("checked", false);
        $("#j4").prop("checked", false);
        $("#j5").prop("checked", false);
        $("#j6").prop("checked", false);
        $("#j7").prop("checked", false);
        $("#j8").prop("checked", false);
        $("#v1").prop("checked", false);
        $("#v2").prop("checked", false);
        $("#v3").prop("checked", false);
        $("#v4").prop("checked", false);
        $("#v5").prop("checked", false);
        $("#v6").prop("checked", false);
        $("#v7").prop("checked", false);
        $("#v8").prop("checked", false);
        $("#s1").prop("checked", false);
        $("#s2").prop("checked", false);
        $("#s3").prop("checked", false);
        $("#s4").prop("checked", false);
    }
    $("#nombre").prop("disabled", estado);
    $("#telefono").prop("disabled", estado);
    $("#nombre").prop("disabled", estado);
    $("#auto").prop("disabled", estado);
    $("#subirfoto").prop("disabled", estado);
    $("#l1").prop("disabled", estado);
    $("#l2").prop("disabled", estado);
    $("#l3").prop("disabled", estado);
    $("#l4").prop("disabled", estado);
    $("#l5").prop("disabled", estado);
    $("#l6").prop("disabled", estado);
    $("#l7").prop("disabled", estado);
    $("#l8").prop("disabled", estado);
    $("#ma1").prop("disabled", estado);
    $("#ma2").prop("disabled", estado);
    $("#ma3").prop("disabled", estado);
    $("#ma4").prop("disabled", estado);
    $("#ma5").prop("disabled", estado);
    $("#ma6").prop("disabled", estado);
    $("#ma7").prop("disabled", estado);
    $("#ma8").prop("disabled", estado);
    $("#m1").prop("disabled", estado);
    $("#m2").prop("disabled", estado);
    $("#m3").prop("disabled", estado);
    $("#m4").prop("disabled", estado);
    $("#m5").prop("disabled", estado);
    $("#m6").prop("disabled", estado);
    $("#m7").prop("disabled", estado);
    $("#m8").prop("disabled", estado);
    $("#j1").prop("disabled", estado);
    $("#j2").prop("disabled", estado);
    $("#j3").prop("disabled", estado);
    $("#j4").prop("disabled", estado);
    $("#j5").prop("disabled", estado);
    $("#j6").prop("disabled", estado);
    $("#j7").prop("disabled", estado);
    $("#j8").prop("disabled", estado);
    $("#v1").prop("disabled", estado);
    $("#v2").prop("disabled", estado);
    $("#v3").prop("disabled", estado);
    $("#v4").prop("disabled", estado);
    $("#v5").prop("disabled", estado);
    $("#v6").prop("disabled", estado);
    $("#v7").prop("disabled", estado);
    $("#v8").prop("disabled", estado);
    $("#s1").prop("disabled", estado);
    $("#s2").prop("disabled", estado);
    $("#s3").prop("disabled", estado);
    $("#s4").prop("disabled", estado);
}
function alertas(tipo, estado, mensaje, tiempo) {
    $("#alerta").html("<div class=\"alert " + tipo + " alert-dismissible\"> <strong > " +
            estado + " </strong> " + mensaje +
            "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>  </div>");
    $("#alerta").show(600);
    setTimeout(function () {
        $("#alerta").hide(600);
    }, tiempo);
}