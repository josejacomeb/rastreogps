<?php
// Start the session
session_start();
if ($_SESSION["usuario"] === NULL) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mapa</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
        <link rel="stylesheet" href="../dist/leaflet-routing-machine.css" />
        <link rel="icon" href="Imagenes_pagina/gps.png">
        <!-- Make sure you put this AFTER Leaflet's CSS -->
        <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
        <script src="../dist/leaflet-routing-machine.js"></script>
        <style type="text/css">
            body,td,th {
                font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 14px;
                color: #FFFFFF;
            }
            body {
                background-image: url(fondo.jpg);
                background-repeat: no-repeat;
                background-size: 100% 100%;
                background-attachment: fixed;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="w3-bar w3-light-grey w3-center">
                <a style="width:30%" class="w3-bar-item w3-mobile"><img src="Imagenes_pagina/logo2.png" style="width:22%"/></a>
                <a href="index.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>INICIO</b></a>
                <a href="mapa.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>MAPA</b></a>
                <a href="historial.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>HISTORIAL</b></a>
                <a href="administracion.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>ADMINISTRACIÓN</b></a>
                <a href="acerca.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>ACERCA</b></a>
            </div>
        </header>

    <container>
        <div class="w3-row-padding">
            <div class="w3-col w3-center w3-mobile" style="width:20%">
                <p><b>Seleccione un vehículo:</b></p>
                <select id="selectorautos" onchange='cambioauto()'>
                    <option value="0">Auto 0</option>
                    <option value="1">Auto 1</option>
                    <option value="2">Auto 2</option>
                    <option value="3">Auto 3</option>
                    <option value="4">Auto 4</option>
                    <option value="5">Auto 5</option>
                    <option value="6">Auto 6</option>
                    <option value="7">Auto 7</option>
                    <option value="8">Auto 8</option>
                    <option value="9">Auto 9</option>
                </select>
                <p id="placa">Placa: </p>
                <p id="modelo">Modelo: </p>
                <p id="clase">Clase: </p>
            </div>
            <div class="w3-col w3-center w3-mobile w3-padding-small" style="width:60%">
                <br/>
                <div class="w3-padding">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="Imagenes_pagina/a1.png" style="width:4%">&nbsp;
                    <button id="botonAbrirpuertas" type="button" class="w3-button w3-card-4 w3-orange w3-text-white w3-mobile">
                        Abrir puertas
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="Imagenes_pagina/a2.png" style="width:4%">&nbsp;
                    <button id="botonCerrarpuertas" type="button" class="w3-button w3-card-4 w3-orange w3-text-white w3-mobile">
                        Cerrar puertas
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="Imagenes_pagina/a4.png" style="width:3%">&nbsp;
                    <button id="botonBloquear" type="button" class="w3-button w3-card-4 w3-orange w3-text-white w3-mobile">
                        Bloquear Motor
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="Imagenes_pagina/a5.png" style="width:4%">&nbsp;
                    <button id="botonDesbloquear" type="button" class="w3-button w3-card-4 w3-orange w3-text-white w3-mobile">
                        Desbloquear Motor
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
                <div class="w3-padding">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="Imagenes_pagina/a3.png" style="width:4%">&nbsp;
                    <button id="botonBusqueda" type="button" class="w3-button w3-card-4 w3-orange w3-text-white w3-mobile">
                        Ubicación
                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="w3-text-white w3-section">  Mostrar ubicación:  </span>
                    <button id="botonCentrar" type="button" class="w3-button w3-card-4 w3-green w3-text-white w3-section">
                        Última
                    </button>
                    <button id="botonTodas" type="button" class="w3-button w3-card-4 w3-green w3-text-white w3-section">
                        Todas
                    </button>&nbsp;&nbsp;
                    <input type="number" name="quantity"
                           min="1" max="100" step="1" value="30" id="maximoMapa" class="w3-cell">
                </div>
            </div>
            <div class="w3-col w3-center w3-mobile " style="width:20%">
                <p class="w3-center"><b>Instructor:</b></p>
                <img id="imageninstructor" src="" style="width:25%">
                <p id="nombreinstructor" class="w3-center">Nombre Instructor</p>
                <p id="telefonoinstructor" class="w3-center">Teléfono</p>
            </div>
         <p id="demo"></p>
        </div>
        <div class="w3-container w3-card-4 w3-center">
            <div id="mapid" style="width:100%; height:475px;  position: relative; outline: currentcolor none medium;" ></div>
        </div>
    </container>
    <footer class="w3-container w3-mobile w3-center">
        <p>Universidad Técnica de Ambato &copy; 2019 Todos los derechos reservados.</p>
    </footer>	 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="pusher.min.js"></script> 
    <script src="mapa.js"></script>
</body></html>
