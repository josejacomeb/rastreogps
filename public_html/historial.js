var body = document.getElementById("divtabla");
var objetorespuesta = [];
var objetounico = [];
var pusher = new Pusher('a0b9a4160e4b7ceab1ef', {
    cluster: 'us2',
    forceTLS: true
});
var fecha1 = document.getElementById("fechainicio");
var fecha2 = document.getElementById("fechafinal");
var channel = pusher.subscribe('cambiodatos');
channel.bind('auto', function (data) {
    obtenerdb($("#seleccion").val());
    if (window.Notification && Notification.permission !== "denied") {
        Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
            var n = new Notification('Información recibida', {
                body: 'El auto ' + data.id + ' ha reportado su ubicación, reviselo en el mapa',
                icon: 'auto.png' // optional
            });
        });
    }
});
var datos = {};
var inicio = false;
function obtenerdb(opcion) {
    var inicio = $("#fechainicio").val().split("-");
    var final = $("#fechafinal").val().split("-");
    $.get("obtenerhistorial.php?q=" + String(opcion) + "&ai=" + inicio[0]
            + "&mi=" + inicio[1] + "&di=" + inicio[2] + "&af=" + final[0]
            + "&mf=" + final[1] + "&df=" + final[2], function (data, status) {
        if (status === "success") {
            if (data.search("Sin datos") > 0) {
                body.innerHTML = data;
            } else {
                body.innerHTML = "";
                objetorespuesta = JSON.parse(data);
                var tabla = document.createElement("table");
                tabla.setAttribute("class", "w3-table w3-striped w3-bordered w3-white");
                tabla.setAttribute("id", "tablahistorial");
                var tblBody = document.createElement("tbody");
                var tblHeader = document.createElement("thead");
                var hilera = document.createElement("tr");
                var textoCelda = document.createTextNode("Nº");
                var celda = document.createElement("td");
                celda.appendChild(textoCelda);
                hilera.appendChild(celda);
                var textoCelda = document.createTextNode("Fecha");
                celda = document.createElement("td");
                celda.appendChild(textoCelda);
                hilera.appendChild(celda);
                var textoCelda = document.createTextNode("Auto");
                celda = document.createElement("td");
                celda.appendChild(textoCelda);
                hilera.appendChild(celda);
                var textoCelda = document.createTextNode("Instructor");
                celda = document.createElement("td");
                celda.appendChild(textoCelda);
                hilera.appendChild(celda);
                var link = document.createElement("a");
                var direccion = "ventanamapa.html?datos=" + JSON.stringify(objetorespuesta);
                link.setAttribute("href", direccion);
                link.innerHTML = 'Latitudes y longitudes';
                link.setAttribute("class", "w3-text-blue");
                celda = document.createElement("td");
                celda.appendChild(link);
                hilera.appendChild(celda);
                tblHeader.appendChild(hilera);
                tabla.appendChild(tblHeader);
                var celda = document.createElement("td");
                var hilera = document.createElement("tr");
                obtenerInstructores();
                for (var x = 0; x < objetorespuesta.length; x++) {
                    var nombreFiltrado = filtrarIntructor(objetorespuesta[x].fecha, objetorespuesta[x].auto);
                    if ($("#instructor").val() === "*" || $("#instructor").val() === nombreFiltrado) {
                        if (objetorespuesta[x].evento === "ira") {
                            var objetounico = [];
                            objetounico.push(objetorespuesta[x]);
                            if (pertenecehorario($("#horario").val(), new Date(objetorespuesta[x].fecha))) {
                                celda = document.createElement("td");
                                hilera = document.createElement("tr");
                                var textoCelda = document.createTextNode(x);
                                celda.appendChild(textoCelda);
                                hilera.appendChild(celda);
                                celda = document.createElement("td");
                                var d = new Date(objetorespuesta[x].fecha);
                                var options = {weekday: 'long', year: 'numeric', month: 'long',
                                    day: 'numeric', hour: "2-digit", minute: "numeric"};
                                var textoCelda = document.createTextNode(d.toLocaleDateString("es-ec", options));
                                celda.appendChild(textoCelda);
                                hilera.appendChild(celda);
                                celda = document.createElement("td");
                                var textoCelda = document.createTextNode(objetorespuesta[x].auto);
                                celda.appendChild(textoCelda);
                                hilera.appendChild(celda);
                                celda = document.createElement("td");
                                var textoCelda = document.createTextNode(nombreFiltrado);
                                celda.appendChild(textoCelda);
                                hilera.appendChild(celda);
                                celda = document.createElement("td");
                                var textoCelda;
                                var link = document.createElement("a");
                                if (objetorespuesta[x].latitud !== "0.0000" &&
                                        objetorespuesta[x].longitud !== "0.0000") {
                                    var direccion = "ventanamapa.html?datos=" + JSON.stringify(objetounico);
                                    link.innerHTML = "[" + objetorespuesta[x].latitud + " " + objetorespuesta[x].longitud + "]";
                                    link.setAttribute("href", direccion);
                                } else {
                                    link.innerHTML = "-";
                                }
                                celda.appendChild(link);
                                hilera.appendChild(celda);
                                tblBody.appendChild(hilera);
                            }
                        }
                    }
                }
                tabla.appendChild(tblBody);
                body.appendChild(tabla);
            }
        }
    });
}


$(document).ready(function () {
    var hoy = new Date();
    var ayer = new Date();
    ayer.setDate(ayer.getDate());
    var di = ayer.getDate();
    var df = hoy.getDate();
    var mi = ayer.getMonth() + 1; //January is 0!
    var mf = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();
    if (di < 10) {
        di = '0' + di;
    }
    if (mi < 10) {
        mi = '0' + mi;
    }
    if (df < 10) {
        df = '0' + df;
    }
    if (mf < 10) {
        mf = '0' + mf;
    }
    var today = yyyy + '-' + mf + '-' + df;
    var ayer = yyyy + '-' + mi + '-' + di;
    $("#fechainicio").val(ayer);
    $("#fechafinal").val(today);
    obtenerInstructores();
    $.get("configuracionadministradores.php?q=*", function (data, status) {
        if (status === "success") {
            if (data.search("Sin datos") > 0) {
                console.log("Sin datos");
            } else {
                $("#alerta").hide();
                datos = JSON.parse(data);
                $("#instructor").append('<option value="*"> Todos los instructores</option>');
                datos.forEach(function (value) {
                    $("#instructor").append('<option value="' + value.nombre + '">' + value.nombre + '</option>');
                });
            }
        }
    });
    $("#seleccion").change(function () {
        obtenerdb($("#seleccion").val());
    });

    $("#fechainicio").change(function () {
        var finicio = new Date($("#fechainicio").val());
        var ffinal = new Date($("#fechafinal").val());
        if (finicio <= ffinal) {
            obtenerdb($("#seleccion").val());
        }
    });
    $("#fechafinal").change(function () {
        var finicio = new Date($("#fechainicio").val());
        var ffinal = new Date($("#fechafinal").val());
        if (finicio <= ffinal) {
            obtenerdb($("#seleccion").val());
        }
    });
    $("#evento").change(function () {
        obtenerdb($("#seleccion").val());
    });
    $("#horario").change(function () {
        obtenerdb($("#seleccion").val());
    });
    $("#instructor").change(function () {
        obtenerdb("*");
    });
});
function filtrarIntructor(fecha, auto) {
    var nombre = "-";
    var fechabusqueda = new Date(fecha);
    var dia = fechabusqueda.getDay();
    var horario = {};
    var hora = fechabusqueda.getHours();
    for (var i = 0; i < datos.length; i++) {
        if (datos[i].auto === auto) {
            if (dia >= 1 && dia <= 5) {
                if (dia === 1) //Lunes
                    horario = JSON.parse(datos[i].lunes);
                else if (dia === 2) //Martes
                    horario = JSON.parse(datos[i].martes);
                if (dia === 3) //Miercoles
                    horario = JSON.parse(datos[i].miercoles);
                else if (dia === 4) //Jueves
                    horario = JSON.parse(datos[i].jueves);
                if (dia === 5) //Viernes
                    horario = JSON.parse(datos[i].viernes);
                if (hora >= 6 && hora < 8) {
                    if (JSON.parse(horario.h1))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                } else if (hora >= 8 && hora < 10) {
                    if (JSON.parse(horario.h2))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }

                } else if (hora >= 10 && hora < 12) {
                    if (JSON.parse(horario.h3))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                } else if (hora >= 12 && hora < 14) {
                    if (JSON.parse(horario.h4))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                } else if (hora >= 14 && hora < 16) {
                    if (JSON.parse(horario.h5))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                } else if (hora >= 16 && hora < 18) {
                    if (JSON.parse(horario.h6))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }

                } else if (hora >= 18 && hora < 20) {
                    if (JSON.parse(horario.h7))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                } else if (hora >= 20 && hora <= 22) {
                    if (JSON.parse(horario.h8))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                }
            } else if (dia === 6) { //Sabado
                horario = JSON.parse(datos[i].sabado);
                if (hora >= 16 && hora < 17) {
                    if (JSON.parse(horario.h1))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                } else if (hora >= 17 && hora < 18) {
                    if (JSON.parse(horario.h2))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }

                } else if (JSON.parse(hora >= 18 && hora < 19)) {
                    if (JSON.parse(horario.h3))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                } else if (hora >= 19 && hora <= 20) {
                    if (JSON.parse(horario.h4))
                    {
                        nombre = datos[i].nombre;
                        break;
                    }
                }
            }
        }
    }
    return nombre;
}

function obtenerInstructores() {
    $.when($.get("configuracionadministradores.php?q=todo*")).done(function (data, status) {
        if (status === "success") {
            datos = JSON.parse(data);
        }
    });
    if (!inicio) {
        setTimeout(function () {
            obtenerdb("*");
            inicio = true;
        }, 200);
    }
}

function pertenecehorario(opcion, dia) {
    var resultado = false;
    if (opcion === "*") {
        resultado = true;
    } else {
        var diasemana = dia.getDay();
        if (diasemana === 6) {
            if (opcion === "h1") {
                if (dia.getHours() >= 16 && dia.getHours() < 17)
                    resultado = true;
            } else if (opcion === "h2") {
                if (dia.getHours() >= 17 && dia.getHours() < 18)
                    resultado = true;
            } else if (opcion === "h3") {
                if (dia.getHours() >= 18 && dia.getHours() < 19)
                    resultado = true;
            } else if (opcion === "h4") {
                if (dia.getHours() >= 19 && dia.getHours() <= 20)
                    resultado = true;
            }
        } else {
            if (opcion === "h1") {
                if (dia.getHours() >= 6 && dia.getHours() < 8)
                    resultado = true;
            } else if (opcion === "h2") {
                if (dia.getHours() >= 8 && dia.getHours() < 10)
                    resultado = true;
            } else if (opcion === "h3") {
                if (dia.getHours() >= 10 && dia.getHours() < 12)
                    resultado = true;
            } else if (opcion === "h4") {
                if (dia.getHours() >= 12 && dia.getHours() < 14)
                    resultado = true;
            } else if (opcion === "h5") {
                if (dia.getHours() >= 14 && dia.getHours() < 16)
                    resultado = true;
            } else if (opcion === "h6") {
                if (dia.getHours() >= 16 && dia.getHours() < 18)
                    resultado = true;
            } else if (opcion === "h7") {
                if (dia.getHours() >= 18 && dia.getHours() < 20)
                    resultado = true;
            } else if (opcion === "h8") {
                if (dia.getHours() >= 20 && dia.getHours() <= 22)
                    resultado = true;
            }
        }
    }
    return resultado;
}



