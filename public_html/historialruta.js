var baseUsuarios = {};
var d = new Date();
var autos = [];
var vectorRuta = new Array();
var jsonquery;
var salidalatitud = 0.0;
var salidalongitud = 0.0;
var zoom = 20;
var marker, marker3;
var marcadores = new Array();
var control = L.Routing.control({
    waypoints: [
        L.latLng(57.74, 11.94),
        L.latLng(57.6792, 11.949)
    ]
});
var options = {weekday: 'long', year: 'numeric', month: 'long',
    day: 'numeric'};
var pusher = new Pusher('a0b9a4160e4b7ceab1ef', {
    cluster: 'us2',
    forceTLS: true
});
// initialize the map on the "map" div with a given center and zoom
var mymap = L.map('mapid');
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoiam9zZWphY29tZWIiLCJhIjoiY2pyenhzNjhlMHk1eTQzbzkyeXNrOTZnNiJ9.55SE4F5kjdNshtre7b8lFw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1Ijoiam9zZWphY29tZWIiLCJhIjoiY2pyenhzNjhlMHk1eTQzbzkyeXNrOTZnNiJ9.55SE4F5kjdNshtre7b8lFw'
}).addTo(mymap);
var greenIcon = new L.Icon({
    iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
var channel = pusher.subscribe('cambiodatos');
$(document).ready(function () {
    cambiarmapa();
    $("#selectorDia").change(function () {
        cambiarmapa();
    });
    $("#selectorHorario").change(function () {
        cambiarmapa();
    });
    $("#selectorautos").change(function () {
        actualizarselector();
        cambiarmapa();
    });
    actualizarselector();
    channel.bind('auto', function (data) {
        if (window.Notification && Notification.permission !== "denied") {
            Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                var n = new Notification('Información recibida', {
                    body: 'El auto ' + data.id + ' ha reportado su ubicación ' + data.latitud + ' ' + data.longitud + ', reviselo en el mapa',
                    icon: 'auto.png' // optional
                });
            });
        }
        $("#selectorautos").val(data.id);
        cambiarmapa();
        actualizarselector();
    });
});
function cambiarmapa() {
    var cadena = "";
    if ($("#selectorDia").val() === '*') {
        cadena = "/obtenerhistorial.php?q=" + String( $("#selectorautos").val()) + "&opcion=ira";
    } else {
        var inicio = $("#selectorDia").val().split("-");
        var final = $("#selectorDia").val().split("-");
        cadena = "obtenerhistorial.php?q=" + String($("#selectorautos").val()) + "&ai=" + inicio[0]
                + "&mi=" + inicio[1] + "&di=" + inicio[2] + "&af=" + final[0]
                + "&mf=" + final[1] + "&df=" + final[2];
    }
    $.get(cadena, function (data, status) {
        if (status === "success") {
            mymap.removeControl(control);
            marcadores.forEach(function (value) {
                mymap.removeLayer(value);
            });
            if (data.search("Sin datos") > 0) {
                $("demo").html(data);
            } else {
                $("demo").html("");
                if (marker !== undefined) {
                    mymap.removeLayer(marker);
                }
                if (marker3 !== undefined) {
                    mymap.removeLayer(marker3);
                }
                jsonquery = JSON.parse(data);
                $("#maximoMapa").prop("max", jsonquery.length);
                $("#maximoMapa").val(jsonquery.length);
                salidalatitud = parseFloat(jsonquery[0].latitud);
                salidalongitud = parseFloat(jsonquery[0].longitud);
                marcadores = new Array();
                var fechasmarcadores = new Array();
                vectorRuta = new Array();
                jsonquery.forEach(function (datos, index) {
                    if (datos !== null) {
                        if ((salidalatitud !== datos.latitud || salidalongitud !== datos.longitud) &&
                                datos.evento === "ira" && pertenecehorario($("#selectorHorario").val(), new Date(datos.fecha))) {
                            vectorRuta.push(L.latLng(datos.latitud, datos.longitud));
                            if (index === 0) {
                                marker3 = L.marker([salidalatitud, salidalongitud], {icon: greenIcon, title: "Última ubicación reportada"});
                            } else {
                                marker3 = L.marker([parseFloat(datos.latitud), parseFloat(datos.longitud)]);
                            }
                            marcadores.push(marker3);
                            fechasmarcadores.push(datos.fecha);
                        }
                    }
                });

                marcadores.forEach(function (value, index) {
                    value.addTo(mymap);
                    value.bindPopup("Localización anterior auto " + $("#selectorautos").val() + " fecha: " + String(fechasmarcadores[index])).openPopup();
                });
                if (marcadores.length > 0) {
                    marcadores[0].bindPopup("Localización requerida auto " + $("#selectorautos").val() + " fecha: " + String(fechasmarcadores[0])).openPopup();
                    var group = new L.featureGroup(marcadores);
                    mymap.fitBounds(group.getBounds());
                    if ($("#selectorDia").val() !== "*") {
                        control = L.Routing.control({
                            waypoints: vectorRuta
                        }).addTo(mymap);
                        control.hide();
                    }
                }
            }
        }
    });
}


function verificarhoras(dia, horas, nombre, foto, telefono, horario) {
    var resultado = false;
    if (horario === "finsemana") {
        if (horas >= 16 && horas < 17) { //Primer turno
            if (dia.h1)
                resultado = true;
        } else if (horas >= 17 && horas < 18) { //Segundo turno
            if (dia.h2)
                resultado = true;
        } else if (horas >= 18 && horas < 19) { //Tercer turno
            if (dia.h3)
                resultado = true;
        } else if (horas >= 19 && horas < 20) { //Cuarto Turno
            if (dia.h4)
                resultado = true;
        }
    } else {
        if (horas >= 6 && horas < 8) { //Primer turno
            if (dia.h1)
                resultado = true;
        } else if (horas >= 8 && horas < 10) { //Segundo turno
            if (dia.h2)
                resultado = true;
        } else if (horas >= 10 && horas < 12) { //Tercer turno
            if (dia.h3)
                resultado = true;
        } else if (horas >= 12 && horas < 14) { //Cuarto Turno
            if (dia.h4)
                resultado = true;
        } else if (horas >= 14 && horas < 16) { //Quinto turno
            if (dia.h5)
                resultado = true;
        } else if (horas >= 16 && horas < 18) { //Sexto turno
            if (dia.h6)
                resultado = true;
        } else if (horas >= 18 && horas < 20) { //Séptimo turno
            if (dia.h7)
                resultado = true;
        } else if (horas >= 20 && horas <= 22) { //Octavo Turno
            if (dia.h8)
                resultado = true;
        }
    }
    if (resultado) {
        $("#nombreinstructor").text(nombre);
        $("#telefonoinstructor").text(telefono);
        $("#imageninstructor").prop("src", foto);
        $("#imageninstructor").prop("alt", foto);
    }
    return resultado;
}


function actualizarselector() {
    $.get("/obtenerhistorial.php?q=" + String($("#selectorautos").val()) + "&opcion=ira", function (data, status) {
        if (status === "success") {
            jsonquery = JSON.parse(data);   
            var fechaanterior = new Date(2018, 11, 24, 10, 33, 30, 0);
            var fechadb = new Date();
            $("#selectorDia").empty();
            $("#selectorDia").append('<option value="*"> Todos los días</option>');
            var strdia = "";
            var dia = 0;
            jsonquery.forEach(function (value, index) {
                fechadb = new Date(value.fecha);
                if (fechadb.getFullYear() !== fechaanterior.getFullYear() ||
                        fechadb.getMonth() !== fechaanterior.getMonth() ||
                        fechadb.getDate() !== fechaanterior.getDate()) {
                    $("#selectorDia").append('<option value="' + fechadb.getFullYear().toString() +
                            '-' + (1 + fechadb.getMonth()).toString() + '-' + (fechadb.getDate()).toString() +
                            '">' + fechadb.toLocaleDateString("es-ec", options) + '</option>');
                    fechaanterior = fechadb;
                }
            });
        }
    });
}

function pertenecehorario(opcion, diastr) {
    var dia = new Date(diastr);
    var resultado = false;
    if (opcion === "*") {
        resultado = true;
    } else {
        var diasemana = dia.getDay();
        var hora = dia.getHours();
        if (diasemana === 6) {
            if (opcion === "h1") {
                if (hora >= 16 && hora < 17)
                    resultado = true;
            } else if (opcion === "h2") {
                if (hora >= 17 && hora < 18)
                    resultado = true;
            } else if (opcion === "h3") {
                if (hora >= 18 && hora() < 19)
                    resultado = true;
            } else if (opcion === "h4") {
                if (hora >= 19 && hora <= 20)
                    resultado = true;
            }
        } else {
            if (opcion === "h1") {
                if (hora >= 6 && hora < 8)
                    resultado = true;
            } else if (opcion === "h2") {
                if (hora >= 8 && hora < 10)
                    resultado = true;
            } else if (opcion === "h3") {
                if (hora >= 10 && hora < 12)
                    resultado = true;
            } else if (opcion === "h4") {
                if (hora >= 12 && hora < 14)
                    resultado = true;
            } else if (opcion === "h5") {
                if (hora >= 14 && hora < 16)
                    resultado = true;
            } else if (opcion === "h6") {
                if (hora >= 16 && hora < 18)
                    resultado = true;
            } else if (opcion === "h7") {
                if (hora >= 18 && hora < 20)
                    resultado = true;
            } else if (opcion === "h8") {
                if (hora >= 20 && hora <= 22)
                    resultado = true;
            }
        }
    }
    return resultado;
}
