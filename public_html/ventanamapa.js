var zoom = 20;
var marcadores = new Array();
var marker, marker3;
// initialize the map on the "map" div with a given center and zoom
var mymap = L.map('mapid');
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoiam9zZWphY29tZWIiLCJhIjoiY2pyenhzNjhlMHk1eTQzbzkyeXNrOTZnNiJ9.55SE4F5kjdNshtre7b8lFw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1Ijoiam9zZWphY29tZWIiLCJhIjoiY2pyenhzNjhlMHk1eTQzbzkyeXNrOTZnNiJ9.55SE4F5kjdNshtre7b8lFw'
}).addTo(mymap);
var greenIcon = new L.Icon({
    iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
$(document).ready(function () {
    var query = window.location.search.substring(1);
    var url_string = "http://http://gpssindicatopillaro.sytes.net/ventana.html?" + query;
    var url = new URL(url_string);
    var c = url.searchParams.get("datos");
    console.log(c);
    var puntos = {};
    if (c !== null) {
        puntos = JSON.parse(c);
        var latitud = 0;
        var longitud = 0;
        puntos.forEach(function (value, index) {
            latitud = parseFloat(value.latitud);
            longitud = parseFloat(value.longitud);
            marker3 = L.marker([latitud, longitud]);
            marcadores.push(marker3);
            if (latitud !== 0 && longitud !== 0) {
                marcadores[index].addTo(mymap);
                marcadores[index].bindPopup("Localización anterior auto " + value.auto + " fecha: " + String(value.fecha)).openPopup();
            }
        });
        mymap.setView([latitud, longitud], zoom);
        var group = new L.featureGroup(marcadores);
        mymap.fitBounds(group.getBounds());
    }
});