<?php

session_start();
if (isset($_POST["usuario"])) {
    $usuario = $_POST["usuario"];
}
if (isset($_POST["contraseña"])) {
    $contraseña = $_POST["contraseña"];
}
if (isset($_POST["contraseña"]) && isset($_POST["contraseña"])) {
    $config = parse_ini_file('../db.ini');
// Create connection
    try {
        $dbh = new PDO('mysql:dbname=' . $config['db'] . ';host=' . $config['servername'] . ';charset=utf8mb4', $config['username'], $config['password']);
        $stmt = $dbh->prepare("SELECT user FROM informacion WHERE user=?  AND pass=?");
        $stmt->execute([$usuario, $contraseña]);
        $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!$arr) {
            $message = "Nombre de usuario incorrecto";
            $_SESSION["error"] = $message;
            header("location: index.php");
        } else {
            $_SESSION["usuario"] = $usuario;
            unset($_SESSION["error"]);
            header("location: mapa.php");
        }
        $stmt = null;
    } catch (Exception $e) {
        error_log($e->getMessage());
        exit('Error de conexión'); //something a user can understand
    }
}


