#!/usr/bin/env python3

import serial
import os, time
import datetime
import sys



if len(sys.argv) >= 3:
    telefonos = [
        "+593981811727",
        "+593990884114",
        "+593981811727",
        "+593981811727",
        "+593981811727",
        "+593981811727",
        "+593981811727",
        "+593981811727",
        "+593981811727"
    ]
    id = int(sys.argv[1])
    mensaje = sys.argv[2]

    # Enable Serial Communication
    port = serial.Serial("/dev/ttyAMA0", baudrate=9600, timeout=0.3)

    def enviardatos(dato):
        """
            Funcion por si se quiere enviar datos
        """
        global port
        print("Comando entrada: " + dato)
        datostr = dato.encode('utf-8')
        port.write(datostr)

    # Transmitting AT Commands to the Modem
    # '\r\n' indicates the Enter key
    delay = 0.1
    enviardatos('AT'+'\r\n')
    time.sleep(delay)
    rcv = port.read(10)
    print(rcv)
    enviardatos('AT+CIURC=0'+'\r\n')
    time.sleep(delay)
    rcv = port.read(10)
    print(rcv)

    enviardatos('ATE0'+'\r\n')      # Disable the Echo
    time.sleep(delay)
    rcv = port.read(10)
    print(rcv)

    enviardatos('AT+CMGF=1'+'\r\n')  # Select Message format as Text mode
    time.sleep(delay)
    rcv = port.read(10)
    print(rcv)

    enviardatos('AT+CNMI=2,1,0,0,0'+'\r\n')   # New SMS Message Indications
    time.sleep(delay)
    rcv = port.read(10)
    print(rcv)

    # Sending a message to a particular Number
    enviardatos('AT+CMGS="'+ telefonos[id] + '"'+'\r\n')
    rcv = port.read(10)
    print(rcv)
    time.sleep(delay)
    while rcv.decode('utf-8').find(">") < 0:
        time.sleep(delay)
        rcv = port.read(10)
        print(rcv)

    enviardatos(mensaje+'\r')  # Message
    rcv = port.read(10)
    print(rcv)

    enviardatos("\x1A") # Enable to send SMS
    while rcv.decode('utf-8').find("OK") < 0:
        enviardatos('\x1a') # Enable to send SMS Control Z enviar
        time.sleep(delay)
        rcv = port.read(10)
        print(rcv)
