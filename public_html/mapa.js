var baseUsuarios = {};
var d = new Date();
var autos = [];

var jsonquery;
var salidalatitud = 0.0;
var salidalongitud = 0.0;
var zoom = 20;
var marker, marker3;
var marcadores = new Array();

var pusher = new Pusher('a0b9a4160e4b7ceab1ef', {
    cluster: 'us2',
    forceTLS: true
});
// initialize the map on the "map" div with a given center and zoom
var mymap = L.map('mapid');
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoiam9zZWphY29tZWIiLCJhIjoiY2pyenhzNjhlMHk1eTQzbzkyeXNrOTZnNiJ9.55SE4F5kjdNshtre7b8lFw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1Ijoiam9zZWphY29tZWIiLCJhIjoiY2pyenhzNjhlMHk1eTQzbzkyeXNrOTZnNiJ9.55SE4F5kjdNshtre7b8lFw'
}).addTo(mymap);
var greenIcon = new L.Icon({
    iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

var channel = pusher.subscribe('cambiodatos');

$(document).ready(function () {
    cambiarmapa();
    actualizarinstructor();
    $.getJSON("autos.json")
            .done(function (json) {
                json.forEach(function (valor) {
                    autos.push(valor);
                });
                actualizardatosauto($("#selectorautos").val());
            })
            .fail(function (jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
            });
    $("#botonBusqueda").click(function () {
        $.get("datosviagprs.php?id=" + $("#selectorautos").val() + "&lat=x&long=x&opcion=rastrear", function (data, status) {
            if (status === "success") {
                $("demo").html("OK Busqueda");
            }
        });
    });
    $("#botonAbrirpuertas").click(function () {
        $.get("datosviagprs.php?id=" + $("#selectorautos").val() + "&lat=x&long=x&opcion=abrir", function (data, status) {
            if (status === "success") {
                $("demo").html("OK Abrir puertas");
            }
        });
    });
    $("#botonCerrarpuertas").click(function () {
        $.get("datosviagprs.php?id=" + $("#selectorautos").val() + "&lat=x&long=x&opcion=cerrar", function (data, status) {
            if (status === "success") {
                $("demo").html("OK Cerrar Puertas");
            }
        });
    });
    $("#botonBloquear").click(function () {
        $.get("datosviagprs.php?id=" + $("#selectorautos").val() + "&lat=x&long=x&opcion=bloquear", function (data, status) {
            if (status === "success") {
                $("demo").html("OK Bloquear");
            }
        });
    });
    $("#botonDesbloquear").click(function () {
        $.get("datosviagprs.php?id=" + $("#selectorautos").val() + "&lat=x&long=x&opcion=desbloquear", function (data, status) {
            if (status === "success") {
                $("demo").html("Ok Desbloquear");
            }
        });
    });
    $("#botonCentrar").click(function () {
        for (var i = marcadores.length - 1; i >= 1; i--) {
            mymap.removeLayer(marcadores[i]);
        }
        var group = new L.featureGroup(marcadores.slice(0, 1));
        mymap.fitBounds(group.getBounds());
    });
    $("#botonTodas").click(function () {
        for (var i = marcadores.length - 1; i >= $("#maximoMapa").val(); i--) {
            mymap.removeLayer(marcadores[i]);
        }
        for (var i = $("#maximoMapa").val() - 1; i >= 0; i--) {
            marcadores[i].addTo(mymap);
            marcadores[i].bindPopup("Localización anterior auto " + $("#selectorautos").val() + " fecha: " + String(jsonquery[i].fecha)).openPopup();
        }
        var group = new L.featureGroup(marcadores.slice(0, $("#maximoMapa").val() - 1));
        mymap.fitBounds(group.getBounds());
    });
    $("#maximoMapa").change(function () {
        for (var i = marcadores.length - 1; i >= $("#maximoMapa").val(); i--) {
            mymap.removeLayer(marcadores[i]);
        }
        for (var i = $("#maximoMapa").val() - 1; i >= 0; i--) {
            marcadores[i].addTo(mymap);
            marcadores[i].bindPopup("Localización anterior auto " + $("#selectorautos").val() + " fecha: " + String(jsonquery[i].fecha)).openPopup();
        }

    });
    channel.bind('auto', function (data) {
        if (window.Notification && Notification.permission !== "denied") {
            Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                var n = new Notification('Información recibida', {
                    body: 'El auto ' + data.id + ' ha reportado su ubicación ' + data.latitud + ' ' + data.longitud + ', reviselo en el mapa',
                    icon: 'auto.png' // optional
                });
            });
        }
        $("#selectorautos").val(data.id);
        cambiarmapa();
        actualizarinstructor();
    });
});

function cambiarmapa() {
    var cadena = "/obtenerhistorial.php?q=" + $("#selectorautos").val() + "&opcion=ira";
    $.get(cadena, function (data, status) {
        if (status === "success") {
            marcadores.forEach(function (value, index) {
                mymap.removeLayer(value);
            });
            if (data.search("Sin datos") > 0) {
                $("demo").html(data);
            } else {
                $("demo").html("");
                if (marker !== undefined) {
                    mymap.removeLayer(marker);
                }

                if (marker3 !== undefined) {
                    mymap.removeLayer(marker3);
                }
                jsonquery = JSON.parse(data);
                $("#maximoMapa").prop("max", jsonquery.length);
                $("#maximoMapa").val(jsonquery.length);
                salidalatitud = parseFloat(jsonquery[0].latitud);
                salidalongitud = parseFloat(jsonquery[0].longitud);
                marker = L.marker([salidalatitud, salidalongitud], {icon: greenIcon, title: "Última ubicación reportada"}).addTo(mymap);
                if (marcadores.length === 0) {
                    marcadores.push(marker);
                } else {
                    marcadores[0] = marker;
                }
                for (var i = 1; i < jsonquery.length; i++) {
                    if (jsonquery[i] !== null) {
                        if (salidalatitud !== jsonquery[i].latitud || salidalongitud !== jsonquery[i].longitud) {
                            marker3 = L.marker([parseFloat(jsonquery[i].latitud), parseFloat(jsonquery[i].longitud)]);
                            if (marcadores.length < i + 1) {
                                marcadores.push(marker3);
                            } else {
                                marcadores[i] = marker3;
                            }
                        }
                    }
                }
                var vectorRuta = new Array();
                jsonquery.forEach(function (value, index) {
                    marcadores[index].addTo(mymap);
                    marcadores[index].bindPopup("Localización anterior auto " + $("#selectorautos").val() + " fecha: " + String(value.fecha)).openPopup();
                });
                mymap.setView([salidalatitud, salidalongitud], zoom);
                marcadores[0].bindPopup("Localización requerida auto " + $("#selectorautos").val() + " fecha: " + String(jsonquery[0].fecha)).openPopup();
            }
        }
    });
}

function cambioauto() {
    cambiarmapa();
    actualizarinstructor();
    actualizardatosauto($("#selectorautos").val());
}


function actualizarinstructor() {
    $.ajax({
        method: "GET",
        url: "configuracionadministradores.php?q=todo*",
        success: function (dato) {
            $("#nombreinstructor").text("Instructor no asignado");
            $("#telefonoinstructor").text("-");
            $("#imageninstructor").prop("src", "imagenes/default.png");
            if (dato.search("Sin datos") > 0) {
                console.log("Sin instructores");
            } else {
                baseUsuarios = JSON.parse(dato);
                for (var i = 0; i < baseUsuarios.length; i++) {
                    var dia = d.getDay();
                    var horas = d.getHours();
                    if (horas > 22 || horas < 6) {
                        $("#nombreinstructor").text("Horario no laborable");
                        $("#telefonoinstructor").text("-");
                        $("#imageninstructor").prop("src", "imagenes/default.png");
                    } else {
                        if (baseUsuarios[i].auto === $("#selectorautos").val()) {
                            if (dia === 1) { //Lunes
                                if (verificarhoras(JSON.parse(baseUsuarios[i].lunes),
                                        horas, baseUsuarios[i].nombre, baseUsuarios[i].foto,
                                        baseUsuarios[i].telefono, "entresemana"))
                                    break;
                            } else if (dia === 2) { //Martes
                                if (verificarhoras(JSON.parse(baseUsuarios[i].martes),
                                        horas, baseUsuarios[i].nombre, baseUsuarios[i].foto,
                                        baseUsuarios[i].telefono, "entresemana"))
                                    break;
                            } else if (dia === 3) { //Miercoles
                                if (verificarhoras(JSON.parse(baseUsuarios[i].miercoles),
                                        horas, baseUsuarios[i].nombre, baseUsuarios[i].foto,
                                        baseUsuarios[i].telefono, "entresemana"))
                                    break;
                            } else if (dia === 4) { //Jueves
                                if (verificarhoras(JSON.parse(baseUsuarios[i].jueves),
                                        horas, baseUsuarios[i].nombre, baseUsuarios[i].foto,
                                        baseUsuarios[i].telefono, "entresemana"))
                                    break;
                            } else if (dia === 5) { //Viernes
                                if (verificarhoras(JSON.parse(baseUsuarios[i].viernes),
                                        horas, baseUsuarios[i].nombre, baseUsuarios[i].foto,
                                        baseUsuarios[i].telefono, "entresemana"))
                                    break;
                            } else if (dia === 6) { //Sábado
                                if (verificarhoras(JSON.parse(baseUsuarios[i].sabado),
                                        horas, baseUsuarios[i].nombre, baseUsuarios[i].foto,
                                        baseUsuarios[i].telefono, "finsemana"))
                                    break;
                            }
                        }
                    }
                }
            }
        }
    });
}

function actualizardatosauto(numero) {
    var placa = autos[parseInt(numero)].placa;
    var modelo = autos[parseInt(numero)].modelo;
    var clase = autos[parseInt(numero)].clase;
    $("#numeroauto").html("<b>Auto: </b>" + autos[parseInt(numero)].auto);
    $("#placa").html("<b>Placa: </b>" + placa);
    $("#modelo").html("<b>Modelo: </b>" + modelo);
    $("#clase").html("<b>Clase: </b>" + clase);
}


function verificarhoras(dia, horas, nombre, foto, telefono, horario) {
    var resultado = false;
    if (horario.search("fin") >= 0) {
        if (horas >= 16 && horas < 17) { //Primer turno
            if (JSON.parse(dia.h1))
                resultado = true;
        } else if (horas >= 17 && horas < 18) { //Segundo turno
            if (JSON.parse(dia.h2))
                resultado = true;
        } else if (horas >= 18 && horas < 19) { //Tercer turno
            if (JSON.parse(dia.h3))
                resultado = true;
        } else if (horas >= 19 && horas <= 20) { //Cuarto Turno
            if (JSON.parse(dia.h4))
                resultado = true;
        }
    } else {
        if (horas >= 6 && horas < 8) { //Primer turno
            if (JSON.parse(dia.h1))
                resultado = true;
        } else if (horas >= 8 && horas < 10) { //Segundo turno
            if (JSON.parse(dia.h2))
                resultado = true;
        } else if (horas >= 10 && horas < 12) { //Tercer turno
            if (JSON.parse(dia.h3))
                resultado = true;
        } else if (horas >= 12 && horas < 14) { //Cuarto Turno
            if (JSON.parse(dia.h4))
                resultado = true;
        } else if (horas >= 14 && horas < 16) { //Tercer turno
            if (JSON.parse(dia.h5))
                resultado = true;
        } else if (horas >= 16 && horas < 18) { //Cuarto Turno
            if (JSON.parse(dia.h6))
                resultado = true;
        } else if (horas >= 18 && horas < 20) { //Tercer turno
            if (JSON.parse(dia.h7))
                resultado = true;
        } else if (horas >= 20 && horas < 22) { //Cuarto Turno
            if (JSON.parse(dia.h8))
                resultado = true;
        }
    }
    if (resultado) {
        $("#nombreinstructor").text(nombre);
        $("#telefonoinstructor").text(telefono);
        $("#imageninstructor").prop("src", foto);
        $("#imageninstructor").prop("alt", foto);
    }
    return resultado;
}
