<?php
// Start the session
session_start();
if ($_SESSION["usuario"] === NULL) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Acerca</title>
	 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="icon" href="Imagenes_pagina/gps.png">
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="leaflet.js"></script>
<style type="text/css">
body,td,th {
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
}
body {
    background-image: url(fondo.jpg);
    background-repeat: no-repeat;
	background-size: 100% 100%;
	background-attachment: fixed;
}
</style>
</head>
<body>
	<header>
	<div class="w3-bar w3-light-grey w3-center">
	<a style="width:30%" class="w3-bar-item w3-mobile"><img src="Imagenes_pagina/logo2.png" style="width:22%"/></a>
  	<a href="index.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>INICIO</b></a>
  	<a href="mapa.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>MAPA</b></a>
  	<a href="historial.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>HISTORIAL</b></a>
	<a href="administracion.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>ADMINISTRACIÓN</b></a>
  	<a href="acerca.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>ACERCA</b></a>
	</div>
	</header>
	
<container>
	<div class="w3-row-padding w3-margin-top w3-mobile w3-center w3-text-white">
	<h5><b>Manual de Usuario</b></h5>
	<div class="w3-bar">
    <a href="https://mega.nz/#!ghoG0YIT!1qzbY5SR6TaRdZqzGEfSe90C0vyCWDkHXlST2dW8o-Y"><img src="Imagenes_pagina/manual.png" width="10%"/></a>
	</div>
  </div>
	<br/>
<table class="w3-table-all w3-modal-content w3-mobile w3-center" style="width:30%">
    <thead>
      <tr class="w3-red w3-hover-red">
        <th class="w3-center">Dispositivo</th>
        <th class="w3-center">Celular</th>
		<th class="w3-center">Dispositivo</th>
        <th class="w3-center">Celular</th>
      </tr>
    </thead>
    <tr class="w3-hover-green">
      <td class="w3-center">Servidor</td>
      <td class="w3-center">0981811727</td>
	  <td class="w3-center">Auto 5</td>
      <td class="w3-center">---</td>
    </tr>
    <tr class="w3-hover-blue">
      <td class="w3-center">Auto 0</td>
      <td class="w3-center">---</td>
	  <td class="w3-center">Auto 6</td>
      <td class="w3-center">0979758671</td>
    </tr>
    <tr class="w3-hover-teal">
      <td class="w3-center">Auto 1</td>
      <td class="w3-center">---</td>
	  <td class="w3-center">Auto 7</td>
      <td class="w3-center">---</td>
    </tr>
   <tr class="w3-hover-amber">
      <td class="w3-center">Auto 2</td>
      <td class="w3-center">---</td>
	  <td class="w3-center">Auto 8</td>
      <td class="w3-center">---</td>
    </tr>
	<tr class="w3-hover-lime">
      <td class="w3-center">Auto 3</td>
      <td class="w3-center">---</td>
	  <td class="w3-center">Auto 9</td>
      <td class="w3-center">---</td>
    </tr>
	<tr class="w3-hover-yellow">
      <td class="w3-center">Auto 4</td>
      <td class="w3-center">---</td>
	  <td class="w3-center">Auto 10</td>
      <td class="w3-center">---</td>
    </tr>
  </table>
	<br/>
	<div class="w3-row-padding w3-margin-top w3-mobile w3-center w3-text-white">
		<h6>El siguiente trabajo de investigación ha sido realizado por el Sr. Joseph Hernández</h6>
		<h6>Contacto: 0996007749</h6>
  </div>
</container>
<footer class="w3-container w3-display-bottommiddle w3-mobile w3-center w3-text-white">
		<p>Universidad Técnica de Ambato &copy; 2019 Todos los derechos reservados.</p>
	</footer>	 

</body></html>

