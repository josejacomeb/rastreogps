-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for debian-linux-gnueabihf (armv8l)
--
-- Host: localhost    Database: usuarios
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `informacion`
--

DROP TABLE IF EXISTS `informacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacion`
--

LOCK TABLES `informacion` WRITE;
/*!40000 ALTER TABLE `informacion` DISABLE KEYS */;
INSERT INTO `informacion` VALUES (1,'sindicatop','1234'),(2,'admin','admin');
/*!40000 ALTER TABLE `informacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructores`
--

DROP TABLE IF EXISTS `instructores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructores` (
  `nombre` varchar(250) NOT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `auto` int(11) DEFAULT NULL,
  `lunes` varchar(1000) DEFAULT NULL,
  `martes` varchar(1000) DEFAULT NULL,
  `miercoles` varchar(1000) DEFAULT NULL,
  `jueves` varchar(1000) DEFAULT NULL,
  `viernes` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructores`
--

LOCK TABLES `instructores` WRITE;
/*!40000 ALTER TABLE `instructores` DISABLE KEYS */;
INSERT INTO `instructores` VALUES ('Barbara Palvin','0777777777777777','imagenes/palvin.jpg',0,'{\"h1\":\"true\",\"h2\":\"true\",\"h3\":\"true\",\"h4\":\"true\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"true\",\"h4\":\"true\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"true\",\"h4\":\"false\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"true\",\"h4\":\"true\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"false\",\"h4\":\"false\"}'),('Elizabeth Olsen','0990884114','imagenes/olsen.jpg',2,'{\"h1\":\"true\",\"h2\":\"false\",\"h3\":\"true\",\"h4\":\"false\",\"h5\":\"false\",\"h6\":\"false\"}','{\"h1\":\"true\",\"h2\":\"true\",\"h3\":\"true\",\"h4\":\"true\",\"h5\":\"true\",\"h6\":\"false\"}','{\"h1\":\"false\",\"h2\":\"true\",\"h3\":\"false\",\"h4\":\"true\",\"h5\":\"false\",\"h6\":\"false\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"false\",\"h4\":\"false\",\"h5\":\"false\",\"h6\":\"false\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"false\",\"h4\":\"false\",\"h5\":\"false\",\"h6\":\"false\"}'),('Emma Roberts','09908841144','imagenes/emmaroberts.png',2,'{\"h1\": \"false\", \"h2\": \"false\", \"h3\": \"true\", \"h4\": \"true\", \"h5\": \"false\", \"h6\": \"false\"}','{\"h1\": \"true\", \"h2\": \"true\", \"h3\": \"true\", \"h4\": \"true\", \"h5\": \"true\", \"h6\": \"true\"}','{\"h1\": \"false\", \"h2\": \"false\", \"h3\": \"true\", \"h4\": \"false\", \"h5\": \"false\", \"h6\": \"false\"}','{\"h1\": \"false\", \"h2\": \"false\", \"h3\": \"false\", \"h4\": \"true\", \"h5\": \"false\", \"h6\": \"false\"}','{\"h1\": \"false\", \"h2\": \"false\", \"h3\": \"false\", \"h4\": \"false\", \"h5\": \"true\", \"h6\": \"false\"}'),('Luis Alvarez','0996007749','imagenes/2017-12-14-0001.jpg',1,'{\"h1\":\"true\",\"h2\":\"true\",\"h3\":\"true\",\"h4\":\"true\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"false\",\"h4\":\"false\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"false\",\"h4\":\"false\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"false\",\"h4\":\"false\"}','{\"h1\":\"false\",\"h2\":\"false\",\"h3\":\"false\",\"h4\":\"false\"}'),('Zooey Deschanel','0990884114','imagenes/zooey.png',3,'{\"h1\": \"true\", \"h2\": \"false\", \"h3\": \"false\", \"h4\": \"false\", \"h5\": \"false\", \"h6\": \"true\"}','{\"h1\": \"false\", \"h2\": \"true\", \"h3\": \"false\", \"h4\": \"false\", \"h5\": \"false\", \"h6\": \"false\"}','{\"h1\": \"false\", \"h2\": \"false\", \"h3\": \"true\", \"h4\": \"false\", \"h5\": \"false\", \"h6\": \"false\"}','{\"h1\": \"false\", \"h2\": \"false\", \"h3\": \"false\", \"h4\": \"true\", \"h5\": \"false\", \"h6\": \"false\"}','{\"h1\": \"true\", \"h2\": \"false\", \"h3\": \"false\", \"h4\": \"false\", \"h5\": \"true\", \"h6\": \"true\"}');
/*!40000 ALTER TABLE `instructores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro` (
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `evento` varchar(6) NOT NULL,
  `auto` int(6) NOT NULL,
  `latitud` float(7,4) DEFAULT NULL,
  `longitud` float(7,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro`
--

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
INSERT INTO `registro` VALUES ('2019-04-12 22:38:23','iswr',0,0.0000,0.0000),('2019-04-12 22:39:51','iswr',0,0.0000,0.0000),('2019-04-12 22:41:46','iswr',0,0.0000,0.0000),('2019-04-12 22:41:55','iswr',0,0.0000,0.0000),('2019-04-12 22:46:36','iswr',0,0.0000,0.0000),('2019-04-12 22:49:25','ira',0,-1.1725,-78.5440),('2019-04-13 00:49:52','ira',0,-1.1725,-78.5439),('2019-04-13 00:51:52','ira',0,-1.1725,-78.5439),('2019-04-13 00:52:03','ira',0,-1.1725,-78.5439),('2019-04-13 00:59:14','ira',0,-1.1725,-78.5440),('2019-04-13 00:59:24','ira',0,-1.1725,-78.5440),('2019-04-13 01:01:47','iswr',0,0.0000,0.0000),('2019-04-13 01:04:22','iswr',0,0.0000,0.0000),('2019-04-13 01:05:18','ira',0,-1.1724,-78.5440),('2019-04-13 01:05:27','ira',0,-1.1724,-78.5440),('2019-04-13 01:05:36','ira',0,-1.1724,-78.5440),('2019-04-13 01:06:02','iswr',0,0.0000,0.0000),('2019-04-13 01:06:43','ira',0,-1.1725,-78.5441),('2019-04-13 01:07:47','ira',0,-1.1725,-78.5441),('2019-04-16 05:20:26','iswa',0,0.0000,0.0000),('2019-04-17 21:46:12','ira',0,-1.1725,-78.5441),('2019-04-17 22:53:08','ira',0,-1.1725,-78.5440),('2019-04-17 22:53:17','ira',0,-1.1725,-78.5440),('2019-04-17 22:54:07','ira',0,-1.1725,-78.5440),('2019-04-17 23:12:01','ira',0,-1.1726,-78.5439),('2019-04-17 23:12:15','ira',0,-1.1726,-78.5439),('2019-04-17 08:33:58','ira',0,-1.1726,-78.5439),('2019-04-17 23:44:16','ira',0,-1.1725,-78.5440),('2019-04-17 23:45:49','ira',0,-1.1726,-78.5439),('2019-04-17 23:48:45','ira',0,-1.1725,-78.5440),('2019-04-17 23:50:01','ira',0,-1.1726,-78.5439),('2019-04-17 08:55:59','ira',0,-1.1726,-78.5439),('2019-04-17 23:57:55','ira',0,-1.1726,-78.5439),('2019-04-17 23:58:49','ira',0,-1.1726,-78.5439),('2019-04-17 23:59:09','ira',0,-1.1726,-78.5439),('2019-04-18 01:30:53','ira',0,-1.1726,-78.5439),('2019-04-18 01:31:14','ira',0,-1.1726,-78.5439),('2019-04-18 01:33:43','ira',0,-1.1726,-78.5439),('2019-04-18 01:33:51','ira',0,-1.1726,-78.5439),('2019-04-18 01:41:32','ira',0,-1.1725,-78.5440),('2019-04-18 00:50:03','ira',0,-1.1724,-78.5440),('2019-04-18 00:50:21','ira',0,-1.1724,-78.5440),('2019-04-18 01:05:44','ira',0,-1.1724,-78.5440),('2019-04-18 01:05:54','ira',0,-1.1724,-78.5440),('2019-04-18 01:06:03','ira',0,-1.1724,-78.5440),('2019-04-18 01:06:13','ira',0,-1.1724,-78.5440),('2019-04-18 02:50:34','ira',0,-1.1725,-78.5437),('2019-04-18 02:51:19','ira',0,-1.1725,-78.5437),('2019-04-18 02:51:28','ira',0,-1.1725,-78.5437),('2019-04-18 04:47:07','ira',0,-1.1725,-78.5440),('2019-04-18 04:56:14','ira',0,-1.1725,-78.5440),('2019-04-18 05:00:45','ira',0,-1.1725,-78.5440),('2019-04-18 05:07:02','ira',0,-1.1725,-78.5440),('2019-04-18 05:07:25','ira',0,-1.1725,-78.5440),('2019-04-18 05:10:52','ira',0,-1.1725,-78.5440),('2019-04-18 05:11:01','ira',0,-1.1725,-78.5440),('2019-04-18 05:11:11','ira',0,-1.1725,-78.5440),('2019-04-18 05:15:44','ira',0,-1.1725,-78.5440),('2019-04-18 05:16:02','ira',0,-1.1725,-78.5440),('2019-04-18 05:18:11','ira',0,-1.1725,-78.5440),('2019-04-18 05:23:41','ira',0,-1.1724,-78.5439),('2019-04-18 05:23:50','ira',0,-1.1724,-78.5439),('2019-04-18 05:43:26','ira',0,-1.1724,-78.5439),('2019-04-18 05:44:38','ira',0,-1.1724,-78.5439),('2019-04-18 05:46:07','ira',0,-1.1724,-78.5439),('2019-04-18 11:00:41','ira',0,-1.1724,-78.5440),('2019-04-18 12:00:41','ira',0,-1.1724,-78.5441),('2019-04-18 12:00:56','ira',0,-1.1724,-78.5441),('2019-04-18 12:20:40','ira',0,-1.1725,-78.5440),('2019-04-18 13:00:41','ira',0,-1.1725,-78.5440),('2019-04-18 13:40:42','ira',0,-1.1725,-78.5440),('2019-04-18 13:40:51','ira',0,-1.1725,-78.5440),('2019-04-18 14:00:43','ira',0,-1.1724,-78.5440),('2019-04-18 14:22:44','ira',0,-1.1724,-78.5440),('2019-04-18 14:25:04','ira',0,-1.1724,-78.5440),('2019-04-18 14:40:45','ira',0,-1.1724,-78.5440),('2019-04-18 14:40:54','ira',0,-1.1724,-78.5440),('2019-04-18 15:00:42','ira',0,-1.1724,-78.5439),('2019-04-18 15:00:51','ira',0,-1.1724,-78.5439),('2019-04-18 15:20:41','ira',0,-1.1724,-78.5439),('2019-04-18 15:20:51','ira',0,-1.1724,-78.5439),('2019-04-18 15:40:55','ira',0,-1.1725,-78.5440),('2019-04-18 16:00:55','ira',0,-1.1724,-78.5439),('2019-04-18 16:20:41','ira',0,-1.1725,-78.5440),('2019-04-18 16:20:51','ira',0,-1.1725,-78.5440),('2019-04-18 16:40:47','ira',0,-1.1725,-78.5440),('2019-04-18 17:00:42','ira',0,-1.1724,-78.5440),('2019-04-18 17:00:56','ira',0,-1.1724,-78.5440),('2019-04-18 17:40:42','ira',0,-1.1725,-78.5439),('2019-04-18 18:00:42','ira',0,-1.1725,-78.5440),('2019-04-18 18:22:48','ira',0,-1.1724,-78.5440),('2019-04-18 18:40:41','ira',0,-1.1725,-78.5440),('2019-04-18 18:40:51','ira',0,-1.1725,-78.5440),('2019-04-18 19:00:49','ira',0,-1.1725,-78.5440),('2019-04-18 19:20:55','ira',0,-1.1725,-78.5440),('2019-04-18 19:40:43','ira',0,-1.1724,-78.5440),('2019-04-18 19:40:52','ira',0,-1.1724,-78.5440),('2019-05-08 08:04:21','ira',0,-1.1726,-78.5439),('2019-05-08 08:09:00','iswr',0,0.0000,0.0000),('2019-05-08 08:10:18','iswr',0,0.0000,0.0000),('2019-05-08 23:10:26','iswa',0,0.0000,0.0000),('2019-05-08 23:10:58','ira',0,-1.1726,-78.5439),('2019-05-08 23:11:07','ira',0,-1.1726,-78.5439),('2019-05-08 23:11:37','iswa',0,0.0000,0.0000),('2019-05-08 23:12:24','iswc',0,0.0000,0.0000),('2019-05-08 23:13:21','iswb',0,0.0000,0.0000),('2019-05-08 23:14:08','iswd',0,0.0000,0.0000),('2019-05-08 23:15:07','iswr',0,0.0000,0.0000),('2019-05-08 23:15:50','ira',0,-1.1726,-78.5439),('2019-05-09 00:26:34','ira',0,-1.1725,-78.5440),('2019-05-11 16:13:27','iswc',0,0.0000,0.0000);
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `pass` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'usuario','usuario','usuario@usuario.usuario'),(2,'usuario1','usuario1','usuario1@usuario1.usuario1');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-11 12:27:49
