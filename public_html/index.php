<?php
session_start();
?>
<!DOCTYPE html> 
<html>
    <head>
        <title>Página de Inicio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="icon" href="Imagenes_pagina/gps.png">
        <style type="text/css">
            body,td,th {
                font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 14px;
                color: #FFFFFF;
            }
            body {
                background-image: url(fondo.jpg);
                background-repeat: no-repeat;
                background-size: 100% 100%;
                background-attachment: fixed;
            }
        </style>
    </head>

    <body>


        <header>
            <div id="home" class="w3-modal-content w3-card-4 w3-display-middle w3-center w3-gray w3-padding-large w3-round-xlarge" style="max-width:400px">
                <img src="Imagenes_pagina/logo.png" width="100%"/>
                <form class="" action="login.php" method="post">
                    <p style="font-size: 16px;"><b>USUARIO:</b></p>
                    <input class="w3-input" name="usuario" placeholder="Ingrese usuario"></input>

                    <p style="font-size: 16px;"><b>CONTRASEÑA:</b></p>
                    <input class="w3-input" type='password' placeholder="Ingrese contraseña" name="contraseña"></input>
                    <?php
                    if (isset($_SESSION["error"])) {
                        $error = $_SESSION["error"];
                        echo "<br/><span class='w3-text-white w3-light-green'> El usuario no existe o no tiene acceso al sistema. </span> <br/>";
                    }
                    ?>  
                    <br/>
                    <button type="submit" class="w3-button w3-block w3-deep-orange"><b>Ingresar</b></button>
                </form>
            </div>
        </header>
        <footer class="w3-container w3-display-bottommiddle w3-mobile">
            <p>Universidad Técnica de Ambato &copy; 2019 Todos los derechos reservados.</p>
        </footer>

    </body>
</html>
