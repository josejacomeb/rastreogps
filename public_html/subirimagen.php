<?php

// En versiones de PHP anteriores a la 4.1.0, debería utilizarse $HTTP_POST_FILES en lugar
// de $_FILES.
session_start();
if ($_SESSION["usuario"] === NULL) {
    echo "Acceso denegado";
} else {
    $dir_subida = getcwd() . '/imagenes/';
    $fichero_subido = $dir_subida . basename($_FILES['fichero_usuario']['name']);

    move_uploaded_file($_FILES['fichero_usuario']['tmp_name'], $fichero_subido);

    echo $_FILES['fichero_usuario']['name'];
}