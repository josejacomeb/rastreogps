<?php
// Start the session
session_start();
if ($_SESSION["usuario"] === NULL) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Administración</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="icon" href="gps.png">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">GPS</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Inicio</a></li>
                        <li><a href="mapa.php">Mapa</a></li>
                        <li><a href="historial.php">Historial</a></li>
                        <li><a href="acerca.php">Acerca</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id='alerta'>
        </div>  
        <div class="row container-fluid">
            <div class="col-sm-2 media">
                <span>Instructores:  </span> <select class="form-control mx-1" id="selectorInstructores"></select>
            </div>
        </div>
        <div class="container-fluid">
            <button class="mx-1 btn btn-primary" type="submit" id="modificar">Modificar</button>
            <button class="btn btn-primary" type="submit" id="resetearvalores">Resetear Valores</button>
            <button class="btn btn-primary" type="submit" id="eliminarusuario">Eliminar usuario</button>
        </div>
        <div class="row container-fluid">
            <div class="col-sm-2 media">
                <div class="media-middle">
                    <img class="media-object  mx-1" id="imagen" src="imagenes/default.png" alt="Foto" style="width:50%">
                    <form  name="formaimagen" id="formaimagen">
                        <input type="file" name="fichero_usuario" class="form-control-file border" id="subirfoto" name="subirfoto">
                    </form>
                </div>
            </div>
            <div class="col-sm-2 container-fluid">
                <label for="pwd">Nombre:</label>
                <input type="text" class="form-control" id="nombre">
                <label for="pwd">Teléfono:</label>
                <input type="text" class="form-control" id="telefono">
                <label for="pwd">Auto:</label>
                <select id="auto">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                </select >
            </div>

            <div class="col-sm-1">
                <strong>Lunes</strong>
                <div class="form-check-input">
                    <label><input type="checkbox" id="l1">06:00-08:00</label>
                    <label><input type="checkbox" id="l2">08:00-10:00</label>
                    <label><input type="checkbox" id="l3">10:00-12:00</label>
                    <label><input type="checkbox" id="l4">12:00-14:00</label>
                    <label><input type="checkbox" id="l5">14:00-16:00</label>
                    <label><input type="checkbox" id="l6">16:00-18:00</label>
                    <label><input type="checkbox" id="l7">18:00-20:00</label>
                    <label><input type="checkbox" id="l8">20:00-22:00</label>
                </div>
            </div>
            <div class="col-sm-1">
                <strong>Martes</strong>
                <div class="form-check-input">
                    <label><input type="checkbox" id="ma1">06:00-08:00</label>
                    <label><input type="checkbox" id="ma2">08:00-10:00</label>
                    <label><input type="checkbox" id="ma3">10:00-12:00</label>
                    <label><input type="checkbox" id="ma4">12:00-14:00</label>
                    <label><input type="checkbox" id="ma5">14:00-16:00</label>
                    <label><input type="checkbox" id="ma6">16:00-18:00</label>
                    <label><input type="checkbox" id="ma7">18:00-20:00</label>
                    <label><input type="checkbox" id="ma8">20:00-22:00</label>
                </div>
            </div>
            <div class="col-sm-1">
                <strong>Miércoles</strong>
                <div class="form-check-input">
                    <label><input type="checkbox" id="m1">06:00-08:00</label>
                    <label><input type="checkbox" id="m2">08:00-10:00</label>
                    <label><input type="checkbox" id="m3">10:00-12:00</label>
                    <label><input type="checkbox" id="m4">12:00-14:00</label>
                    <label><input type="checkbox" id="m5">14:00-16:00</label>
                    <label><input type="checkbox" id="m6">16:00-18:00</label>
                    <label><input type="checkbox" id="m7">18:00-20:00</label>
                    <label><input type="checkbox" id="m8">20:00-22:00</label>
                </div>
            </div>
            <div class="col-sm-1">
                <strong>Jueves</strong>
                <div class="form-check-input">
                    <label><input type="checkbox" id="j1">06:00-08:00</label>
                    <label><input type="checkbox" id="j2">08:00-10:00</label>
                    <label><input type="checkbox" id="j3">10:00-12:00</label>
                    <label><input type="checkbox" id="j4">12:00-14:00</label>
                    <label><input type="checkbox" id="j5">14:00-16:00</label>
                    <label><input type="checkbox" id="j6">16:00-18:00</label>
                    <label><input type="checkbox" id="j7">18:00-20:00</label>
                    <label><input type="checkbox" id="j8">20:00-22:00</label>
                </div>
            </div>
            <div class="col-sm-1">
                <strong>Viernes</strong>
                <div class="form-check-input">
                    <label><input type="checkbox" id="v1">06:00-08:00</label>
                    <label><input type="checkbox" id="v2">08:00-10:00</label>
                    <label><input type="checkbox" id="v3">10:00-12:00</label>
                    <label><input type="checkbox" id="v4">12:00-14:00</label>  
                    <label><input type="checkbox" id="v5">14:00-16:00</label>
                    <label><input type="checkbox" id="v6">16:00-18:00</label>
                    <label><input type="checkbox" id="v7">18:00-20:00</label>
                    <label><input type="checkbox" id="v8">20:00-22:00</label>
                </div>
            </div>
            <div class="col-sm-1">
                <strong>Sábado</strong>
                <div class="form-check-input">
                    <label><input type="checkbox" id="s1">16:00-17:00</label>
                    <label><input type="checkbox" id="s2">17:00-18:00</label>
                    <label><input type="checkbox" id="s3">18:00-19:00</label>
                    <label><input type="checkbox" id="s4">19:00-20:00</label>            
                </div>
            </div>
        </div> 

        <script src="administracion.js"></script>
    </body>
</html> 