<?php
// Start the session
session_start();
if ($_SESSION["usuario"] === NULL) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Historial</title>
	 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="icon" href="Imagenes_pagina/gps.png">
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="leaflet.js"></script>
<style type="text/css">
body,td,th {
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #FFFFFF;
}
body {
    background-image: url(fondo.jpg);
    background-repeat: no-repeat;
	background-size: 100% 100%;
	background-attachment: fixed;
}
</style>
</head>
<body>
	<header>
	<div class="w3-bar w3-light-grey w3-center">
	<a style="width:30%" class="w3-bar-item w3-mobile"><img src="Imagenes_pagina/logo2.png" style="width:22%"/></a>
  	<a href="index.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>INICIO</b></a>
  	<a href="mapa.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>MAPA</b></a>
  	<a href="historial.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>HISTORIAL</b></a>
	<a href="administracion.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>ADMINISTRACIÓN</b></a>
  	<a href="acerca.php" style="width:14%" class="w3-bar-item w3-button w3-mobile w3-hover-red w3-padding-large"><b>ACERCA</b></a>
	</div>
	</header>
	
<container>
	<div class="w3-row w3-padding-64">
		<div class="w3-col" style="width:15%"><p></p></div>
  		<div class="w3-col w3-border w3-center w3-hover-shadow w3-mobile" style="width:25%">
		<div class="w3-deep-orange w3-xlarge w3-padding-32">Búsqueda General</div>
		<img src="Imagenes_pagina/b1.png" class="w3-padding-24" width="60%"/>
		<div class="w3-lime w3-padding-24">
		<a href="historial_general.php" class="w3-button w3-green w3-padding-large w3-hover-shadow"><b>Ingresar</b></a>
    	</div>	
		</div>
		<div class="w3-col" style="width:20%"><p></p></div>
  		<div class="w3-col w3-border w3-center w3-hover-shadow w3-mobile" style="width:25%">
		<div class="w3-deep-orange w3-xlarge w3-padding-32">Búsqueda por Ruta</div>
		<img src="Imagenes_pagina/b2.png" class="w3-padding-24" width="60%"/>
		<div class="w3-lime w3-padding-24">
      	<a href="historial_ruta.php" class="w3-button w3-green w3-padding-large w3-hover-shadow"><b>Ingresar</b></a>
    	</div>
		</div>
		<div class="w3-col" style="width:15%"><p></p></div>
	</div>
</container>
<footer class="w3-container w3-display-bottommiddle w3-mobile w3-center">
		<p>Universidad Técnica de Ambato &copy; 2019 Todos los derechos reservados.</p>
	</footer>	 

</body></html>
